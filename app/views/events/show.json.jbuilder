json.extract! @event, :id, :title, :description, :start_time, :end_time,
              :submission_time, :decision_time,
              :place_name, :address, :city, :region, :locality, :url, :contact,
              :tag_list
