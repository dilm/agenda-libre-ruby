# Supervise all the actions done
class VersionsController < ApplicationController
  def index
    respond_to do |format|
      format.html do
        @search = PaperTrail::Version.search params[:q]
        @search.sorts = 'created_at desc' if @search.sorts.empty?
        @versions = @search.result.page params[:page]
      end
      format.rss do
        @versions = PaperTrail::Version.order('created_at desc').limit(20)
      end
    end
  end
end
