# Manage regions, mostly get stats out of them
class RegionsController < ApplicationController
  has_scope :region

  before_action :set_regions, only: [:index]

  def index; end

  private

  def set_regions
    @regions = apply_scopes Region.local
  end
end
