$(document).on 'turbolinks:load', ->
	return unless navigator.geolocation

	$('.digest .filter:first-child').each ->
		$(this).before '
			<a id="nearMe" href="#" title="⯐">
				<em class="fa fa-2x fa-street-view"></em>
			</a>'
		$('#nearMe').click (event) ->
			event.preventDefault()
			navigator.geolocation.getCurrentPosition showPosition, (error) ->
				switch error.code
					when error.PERMISSION_DENIED
						$('#nearMe').remove()

showPosition = (position) ->
	coords = "[#{position.coords.latitude}, #{position.coords.longitude}]"
	url = location.href
	if location.search
		url = url.replace 'near=', ''
		url += '&'
	else
		url += '?'
	url += "near[location]=#{coords}&near[distance]=20"
	location.replace url
