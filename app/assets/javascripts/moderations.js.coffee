$(document).on 'turbolinks:load', ->
  $('body.moderations .radios label').click ->
    $('body.moderations #event_reason').parent().slideUp()
  $('body.moderations .radios label:last-child').click ->
    $('body.moderations #event_reason').parent().slideDown()
