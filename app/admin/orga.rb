ActiveAdmin.register Orga do
  permit_params :active, :kind_id, :name, :description, :url, :city, :region_id

  scope :all
  scope :active, default: true
  scope :inactive
  scope :unmoderated

  config.sort_order = 'updated_at_desc'

  filter :kind, as: :check_boxes
  filter :name
  filter :region

  index do
    column :id do |orga|
      link_to orga.id, orga
    end
    column :kind
    column :name
    column :city
    column :region
    column :url
    column :feed
    column :contact
    column :submitter
    column :updated_at
    column :active
    actions
  end

  csv do
    column(:kind) { |o| o.kind.name }
    column :name
    column :city
    column :region
    column :url
    column :feed
    column :contact
    column :submitter
    column :updated_at
    column :active
  end

  show do |l|
    attributes_table do
      row :kind
      row :name
      row :address
      row :city
      row :region
      h3 Orga.human_attribute_name :description
      section do
        simple_format l.description
      end
      row :url do
        link_to l.url, l.url
      end
      row :feed do
        link_to l.feed, l.feed
      end
      row :contact do
        mail_to l.contact, l.contact
      end
      row :active
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors
    f.inputs do
      f.input :kind
      f.input :name
      f.input :address, as: :string
      f.input :city
      f.input :region
      f.input :latitude
      f.input :longitude
      f.input :description
      f.input :url
      f.input :feed
      f.input :contact
      f.input :submitter
      f.input :active
    end
    f.actions
  end
end
