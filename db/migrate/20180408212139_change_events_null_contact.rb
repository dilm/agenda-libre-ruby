# Events contacts are no longer required
class ChangeEventsNullContact < ActiveRecord::Migration[5.1]
  def up
    change_column :events, :contact, :string, default: '', null: true
  end

  def down
    change_column :events, :contact, :string, default: '', null: false
  end
end
