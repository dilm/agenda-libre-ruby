require 'test_helper'

# Mostly a data holder model, to maintain coutry > region data
class RegionTest < ActiveSupport::TestCase
  test 'proper timezone' do
    assert regions(:region_one).tzid.present?
    assert regions(:region_one).tzid == 'Europe/Paris'
  end

  test 'check québec timezone is local timezone' do
    assert regions(:region_quebec).tzid == Time.now.zone
  end

  test 'check other timezone is local timezone' do
    assert regions(:region_other).tzid == 'CEST'
  end
end
